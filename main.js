var pageCounter = 1;
var moduleContainer = document.getElementById('module-info');
var btn = document.getElementById("btn");

btn.addEventListener("click", function(){
  var ourRequest = new XMLHttpRequest();
  ourRequest.open('GET', 'https://raw.githubusercontent.com/profharimohanpandey/CW2/master/module-'+ pageCounter +'.json');
  ourRequest.onload = function(){
    //console.log(ourRequest.responseText);
    var ourData = JSON.parse(ourRequest.responseText);
    //console.log(ourData[0]);
    renderHTML(ourData);
  };
  ourRequest.send();
pageCounter++;
if (pageCounter > 3){
//btn.classList.add("hide-me");
  btn.disabled = true;
}
});

function renderHTML(data){
  var htmlString = "";

  for(i = 0; i < data.length; i++){
    htmlString += "<p>" + data[i].Name + " is a " + data[i].Course + " has assements "; //".</p>";
    for(ii = 0; ii < data[i].Module.Assignment.length; ii++){
      if (ii == 0){
        htmlString += data[i].Module.Assignment[ii];
      } else {
        htmlString += " and " + data[i].Module.Assignment[ii];
      }
    }
    htmlString += ' and Learning Outcome ';
    for(ii = 0; ii < data[i].Module.Learning_outcomes.length; ii++){
      if (ii == 0){
        htmlString += data[i].Module.Learning_outcomes[ii];
      } else {
        htmlString += " and " + data[i].Module.Learning_outcomes[ii];
      }
    }

    htmlString += ' and Volume ';
    for(ii = 0; ii < data[i].Module.Volume.length; ii++){
      if (ii == 0){
        htmlString += data[i].Module.Volume[ii];
      } else {
        htmlString += " and " + data[i].Module.Volume[ii];
      }
    }

    htmlString += ' and weights ';
    for(ii = 0; ii < data[i].Module.weights.length; ii++){
      if (ii == 0){
        htmlString += data[i].Module.weights[ii];
      } else {
        htmlString += " and " + data[i].Module.weights[ii];
      }
    }
    htmlString += '.</p>';
  }
  moduleContainer.insertAdjacentHTML('beforeend', htmlString);

}

//degree programmer js parts start here

var selectedRow = null;

function onFormSubmit(){
       
        var formData = readFormData();
        if(selectedRow == null)
            insertNewRecord(formData);
        else
          updateRecord(formData);

          resetForm();
        
}

function readFormData(){
    var formData ={};
    formData["identificationCode"] =document.getElementById("identificationCode").value;
    formData["name"] =document.getElementById("name").value;
    formData["lo"] =document.getElementById("lo").value;
    formData["exit"] =document.getElementById("exit").value;
    formData["academic"] =document.getElementById("academic").value;
    formData["dmodule"] =document.getElementById("dmodule").value;
    return formData;
}

function insertNewRecord(data){
    var table = document.getElementById("employeeList").getElementsByTagName('tbody')[0];
    var newRow = table.insertRow(table.length);
    cell1 = newRow.insertCell(0);
    cell1.innerHTML = data.identificationCode;
    cell2 = newRow.insertCell(1);
    cell2.innerHTML = data.name;
    cell3 = newRow.insertCell(2);
    cell3.innerHTML = data.lo;
    cell4 = newRow.insertCell(3);
    cell4.innerHTML = data.exit;
    cell5 = newRow.insertCell(4);
    cell5.innerHTML = data.academic;
    cell6 = newRow.insertCell(5);
    cell6.innerHTML = data.dmodule;
    cell7 = newRow.insertCell(6);
    cell7.innerHTML =  `<a class="btn btn-sn btn-outline-danger" onClick="onEdit(this)" href="#">Edit</a>
                         <a class="btn btn-sn btn-outline-danger" onClick="onDelete(this)" href="#">Delete</a>`;    
}

function resetForm(){
    document.getElementById("identificationCode").value = "";
    document.getElementById("name").value = "";
    document.getElementById("lo").value = "";
    document.getElementById("exit").value = "";
    document.getElementById("academic").value = "";
    document.getElementById("dmodule").value = "";
    selectedRow = null;
}

function onEdit(td){
    selectedRow = td.parentElement.parentElement;
    document.getElementById("identificationCode").value = selectedRow.cells[0].innerHTML;
    document.getElementById("name").value = selectedRow.cells[1].innerHTML;
    document.getElementById("lo").value = selectedRow.cells[2].innerHTML;
    document.getElementById("exit").value = selectedRow.cells[3].innerHTML;
    document.getElementById("academic").value = selectedRow.cells[4].innerHTML;
    document.getElementById("dmodule").value = selectedRow.cells[5].innerHTML;
}

function updateRecord(formData){
    selectedRow.cells[0].innerHTML = formData.identificationCode;
    selectedRow.cells[1].innerHTML = formData.name;
    selectedRow.cells[2].innerHTML = formData.lo;
    selectedRow.cells[3].innerHTML = formData.exit;
    selectedRow.cells[4].innerHTML = formData.academic;
    selectedRow.cells[5].innerHTML = formData.dmodule;
}

function onDelete(td){
    if(confirm('Are you sure do you want to delete this record?')){
        row = td.parentElement.parentElement;
        document.getElementById("employeeList").deleteRow(row.rowIndex);
        resetForm();
  }
}


//end of degree page js..

//Here start of js module page...

var selectedRows = null;

function onFormSubmit(){
       
        var formDatas = readFormDatas();
        if(selectedRows == null)
            insertNewRecords(formDatas);
        else
          updateRecords(formDatas);

          resetForms();
        
}

function readFormDatas(){
    var formDatas ={};
    formDatas["identificationCodem"] =document.getElementById("identificationCodem").value;
    formDatas["namem"] =document.getElementById("namem").value;
    formDatas["hours"] =document.getElementById("hours").value;
    formDatas["lom"] =document.getElementById("lom").value;
    formDatas["credits"] =document.getElementById("credits").value;
    return formDatas;
}

function insertNewRecords(data){
    var tables = document.getElementById("employeeListm").getElementsByTagName('tbody')[0];
    var newRows = tables.insertRow(tables.length);
    cell1 = newRows.insertCell(0);
    cell1.innerHTML = data.identificationCodem;
    cell2 = newRows.insertCell(1);
    cell2.innerHTML = data.namem;
    cell3 = newRows.insertCell(2);
    cell3.innerHTML = data.hours;
    cell4 = newRows.insertCell(3);
    cell4.innerHTML = data.lom;
    cell5 = newRows.insertCell(4);
    cell5.innerHTML = data.credits;
    cell5 = newRows.insertCell(5);
    cell5.innerHTML =  `<a class="btn btn-sn btn-outline-danger" onClick="onEdits(this)" href="#">Edit</a>
                         <a class="btn btn-sn btn-outline-danger" onClick="onDeletes(this)" href="#">Delete</a>`;    
}

function resetForms(){
    document.getElementById("identificationCodem").value = "";
    document.getElementById("namem").value = "";
    document.getElementById("hours").value = "";
    document.getElementById("lom").value = "";
    document.getElementById("credits").value = "";
    selectedRow = null;
}

function onEdits(td){
    selectedRows = td.parentElement.parentElement;
    document.getElementById("identificationCodem").value = selectedRows.cells[0].innerHTML;
    document.getElementById("namem").value = selectedRows.cells[1].innerHTML;
    document.getElementById("hours").value = selectedRows.cells[2].innerHTML;
    document.getElementById("lom").value = selectedRows.cells[3].innerHTML;
    document.getElementById("credits").value = selectedRows.cells[4].innerHTML;
}

function updateRecords(formDatas){
    selectedRows.cells[0].innerHTML = formDatas.identificationCodem;
    selectedRows.cells[1].innerHTML = formDatas.namem;
    selectedRows.cells[2].innerHTML = formDatas.hours;
    selectedRows.cells[3].innerHTML = formDatas.lom;
    selectedRows.cells[4].innerHTML = formDatas.credits;
}

function onDeletes(td){
    if(confirm('Are you sure do you want to delete this record?')){
        row = td.parentElement.parentElement;
        document.getElementById("employeeListm").deleteRow(row.rowIndex);
        resetForms();
  }
}

//end of js module page

//start here of js assessment page

var selectedRowa = null;

function onFormSubmit(){
       
        var formDataa = readFormDatas();
        if(selectedRowa == null)
            insertNewRecords(formDataa);
        else
          updateRecords(formDataa);

          resetForms();
        
}

function readFormDatas(){
    var formDataa ={};
    var d = new Date();
    formDataa["modulea"] =document.getElementById("modulea").value;
    formDataa["titlea"] =document.getElementById("titlea").value;
    formDataa["semester"] =document.getElementById("semester").value;
    formDataa["birthday"] =document.getElementById("birthday").value;
    formDataa["room"] =document.getElementById("room").value;
    formDataa["time"] =document.getElementById("time").value;
    formDataa["birthday"] = d.toLocaleDateString('en-GB', document.getElementById("birthday").value);
    return formDataa;
}

function insertNewRecords(data){
    var tablea = document.getElementById("employeeLista").getElementsByTagName('tbody')[0];
    var newRowa = tablea.insertRow(tablea.length);
    cell1 = newRowa.insertCell(0);
    cell1.innerHTML = data.modulea;
    cell2 = newRowa.insertCell(1);
    cell2.innerHTML = data.titlea;
    cell3 = newRowa.insertCell(2);
    cell3.innerHTML = data.semester;
    cell4 = newRowa.insertCell(3);
    cell4.innerHTML = data.birthday;
    cell5 = newRowa.insertCell(4);
    cell5.innerHTML = data.room;
    cell6 = newRowa.insertCell(5);
    cell6.innerHTML = data.time;
    cell7 = newRowa.insertCell(6);
    cell7.innerHTML = data.birthday;
    cell7 = newRowa.insertCell(7);
    cell7.innerHTML =  `<a class="btn btn-sn btn-outline-danger" onClick="onEdita(this)" href="#">Edit</a>
                         <a class="btn btn-sn btn-outline-danger" onClick="onDeletea(this)" href="#">Delete</a>`;    
}

function resetForms(){
    document.getElementById("modulea").value = "";
    document.getElementById("titlea").value = "";
    document.getElementById("semester").value = "";
    document.getElementById("birthday").value = "";
    document.getElementById("room").value = "";
    document.getElementById("time").value = "";
    document.getElementById("birthday").value = "";
    selectedRowa = null;
}

function onEdita(td){
    selectedRowa = td.parentElement.parentElement;
    document.getElementById("modulea").value = selectedRowa.cells[0].innerHTML;
    document.getElementById("titlea").value = selectedRowa.cells[1].innerHTML;
    document.getElementById("semester").value = selectedRowa.cells[2].innerHTML;
    document.getElementById("birthday").value = selectedRowa.cells[3].innerHTML;
    document.getElementById("room").value = selectedRowa.cells[4].innerHTML;
    document.getElementById("time").value = selectedRowa.cells[5].innerHTML;
    document.getElementById("birthday").value = selectedRowa.cells[6].innerHTML;
}

function updateRecords(formDataa){
    selectedRowa.cells[0].innerHTML = formDataa.modulea;
    selectedRowa.cells[1].innerHTML = formDataa.titlea;
    selectedRowa.cells[2].innerHTML = formDataa.semester;
    selectedRowa.cells[3].innerHTML = formDataa.birthday;
    selectedRowa.cells[4].innerHTML = formDataa.room;
    selectedRowa.cells[5].innerHTML = formDataa.time;
    selectedRowa.cells[6].innerHTML = formDataa.birthday;
}

function onDeletea(td){
    if(confirm('Are you sure do you want to delete this record?')){
        row = td.parentElement.parentElement;
        document.getElementById("employeeLista").deleteRow(row.rowIndex);
        resetForms();
  }
}

//end of js assessment page...

//start here of js slot page

var selectedRowt = null;

function onFormSubmit(){
       
        var formDatat = readFormDatas();
        if(selectedRowt == null)
            insertNewRecords(formDatat);
        else
          updateRecords(formDatat);

          resetForms();
        
}

function readFormDatas(){
    var formDatat ={};
    var d = new Date();
    formDatat["modulet"] =document.getElementById("modulet").value;
    formDatat["academict"] =document.getElementById("academict").value;
    formDatat["semester"] =document.getElementById("semester").value;
    formDatat["birthday"] = d.toLocaleDateString('en-GB', document.getElementById("birthday").value);
    formDatat["room"] =document.getElementById("room").value;
    formDatat["time"] =document.getElementById("time").value;
    formDatat["duration"] =document.getElementById("duration").value;
    return formDatat;
}

function insertNewRecords(data){
    var tablet = document.getElementById("employeeListt").getElementsByTagName('tbody')[0];
    var newRowt = tablet.insertRow(tablet.length);
    cell1 = newRowt.insertCell(0);
    cell1.innerHTML = data.modulet;
    cell2 = newRowt.insertCell(1);
    cell2.innerHTML = data.academict;
    cell3 = newRowt.insertCell(2);
    cell3.innerHTML = data.semester;
    cell4 = newRowt.insertCell(3);
    cell4.innerHTML = data.birthday;
    cell5 = newRowt.insertCell(4);
    cell5.innerHTML = data.room;
    cell6 = newRowt.insertCell(5);
    cell6.innerHTML = data.time;
    cell7 = newRowt.insertCell(6);
    cell7.innerHTML = data.duration;
    cell7 = newRowt.insertCell(7);
    cell7.innerHTML =  `<a class="btn btn-sn btn-outline-danger" onClick="onEditt(this)" href="#">Edit</a>
                         <a class="btn btn-sn btn-outline-danger" onClick="onDeletet(this)" href="#">Delete</a>`;    
}

function resetForms(){
    document.getElementById("modulet").value = "";
    document.getElementById("academict").value = "";
    document.getElementById("semester").value = "";
    document.getElementById("birthday").value = "";
    document.getElementById("room").value = "";
    document.getElementById("time").value = "";
    document.getElementById("duration").value = "";
    selectedRowt = null;
}

function onEditt(td){
    selectedRowt = td.parentElement.parentElement;
    document.getElementById("modulet").value = selectedRowt.cells[0].innerHTML;
    document.getElementById("academict").value = selectedRowt.cells[1].innerHTML;
    document.getElementById("semester").value = selectedRowt.cells[2].innerHTML;
    document.getElementById("birthday").value = selectedRowt.cells[3].innerHTML;
    document.getElementById("room").value = selectedRowt.cells[4].innerHTML;
    document.getElementById("time").value = selectedRowt.cells[5].innerHTML;
    document.getElementById("duration").value = selectedRowt.cells[6].innerHTML;
}

function updateRecords(formDatat){
    selectedRowt.cells[0].innerHTML = formDatat.modulet;
    selectedRowt.cells[1].innerHTML = formDatat.academict;
    selectedRowt.cells[2].innerHTML = formDatat.semester;
    selectedRowt.cells[3].innerHTML = formDatat.birthday;
    selectedRowt.cells[4].innerHTML = formDatat.room;
    selectedRowt.cells[5].innerHTML = formDatat.time;
    selectedRowt.cells[6].innerHTML = formDatat.duration;
}

function onDeletet(td){
    if(confirm('Are you sure do you want to delete this record?')){
        row = td.parentElement.parentElement;
        document.getElementById("employeeListt").deleteRow(row.rowIndex);
        resetForms();
  }
}